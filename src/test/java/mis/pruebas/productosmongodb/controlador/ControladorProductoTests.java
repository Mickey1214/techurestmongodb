package mis.pruebas.productosmongodb.controlador;

import mis.pruebas.productosmongodb.modelo.Producto;
import mis.pruebas.productosmongodb.seguridad.JwtBuilder;
import mis.pruebas.productosmongodb.servicio.ServicioProducto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ContextConfiguration(classes={ServicioProducto.class})
@RunWith(SpringRunner.class)
public class ControladorProductoTests {

    @Mock
    ServicioProducto servicioProducto;

    @Mock
    JwtBuilder jwtBuilder;

    @InjectMocks
    ControladorProducto controladorProducto;


    @Test
    public void testObtenerProductos() {
        // Prerequisitos. Datos existentes en la capa de servicio.
        Mockito.when(servicioProducto.obtenerTodos()).thenReturn(Arrays.asList(
                new Producto("Uno", "Zapatillas", 350.00, 200),
                new Producto("Dos", "Remera", 500.00, 100),
                new Producto("Tres", "Cinturón", 600.00, 100),
                new Producto("Cuatro", "Sandalias", 700.00, 300)
        ));

        // Ahora sí, estas son las pruebas.
        final List<Producto> pp = controladorProducto.obtenerProductos(null, null);

        assertNotNull(pp);
        assertFalse(pp.isEmpty());
        assertEquals(4, pp.size());
        assertEquals("Zapatillas", pp.get(0).getNombre());
        assertEquals("Remera", pp.get(1).getNombre());
        assertEquals("Cinturón", pp.get(2).getNombre());
        assertEquals("Sandalias", pp.get(3).getNombre());

    }

}
