package mis.pruebas.productosmongodb.servicio;

import mis.pruebas.productosmongodb.modelo.Producto;

import java.util.List;

public interface RepositorioProductoPersonalizado {
    public List<Producto> buscarPorRangoPreciosPersonalizado(double min, double max);

    public void ajustarPrecioProducto(String id, double precio);
}
