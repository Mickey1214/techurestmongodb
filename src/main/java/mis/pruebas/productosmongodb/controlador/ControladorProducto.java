package mis.pruebas.productosmongodb.controlador;

import mis.pruebas.productosmongodb.modelo.Producto;
import mis.pruebas.productosmongodb.seguridad.JwtBuilder;
import mis.pruebas.productosmongodb.servicio.ServicioProducto;
import org.jose4j.jwt.JwtClaims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/almacen/v2/productos")
public class ControladorProducto {

    @Value("${autenticacion.emisor}")
    private String emisor;

    @Value("${autenticacion.audiencia}")
    private String audiencia;

    @Value("${autenticacion.asunto}")
    private String asunto;

    @Value("${autenticacion.secreto}")
    private String secreto;

    @Value("${autenticacion.expiracion}")
    private float expiracion;


    @Autowired
    ServicioProducto servicioProducto;

    @Autowired
    JwtBuilder jwtBuilder;

    // GET []
    @GetMapping
    public List<Producto> obtenerProductos(@RequestParam(required = false) Double maxPrecio,
                                           @RequestParam(required = false) String nombreProveedor) {
        System.out.println(String.format("==== maxPrecio = %f, nombreProveedor = '%s'", maxPrecio, nombreProveedor));

        if(nombreProveedor != null)
            return this.servicioProducto.buscarPorProveedor(nombreProveedor);

        if(maxPrecio != null)
            return this.servicioProducto.buscarPorRangoPrecio(0.0, maxPrecio.doubleValue());

        return this.servicioProducto.obtenerTodos();

    }

    // GET {id}
    @GetMapping("/{id}")
    public Producto obtenerProductoPorId(@PathVariable String id) {
        return this.servicioProducto.buscarPorId(id);
    }

    // POST
    @PostMapping
    public void guardarProducto(@RequestHeader("Authorization") String autoriz,
                                @RequestBody Producto p) {

        validarAutorizacion(autoriz);

        p.setId(null);
        this.servicioProducto.agregar(p);
    }

    // PUT
    @PutMapping("/{id}")
    public void reemplazarProducto(@RequestHeader("Authorization") String autoriz,
                                   @PathVariable String id,
                                   @RequestBody Producto p) {

        validarAutorizacion(autoriz);

        final Producto o = this.servicioProducto.buscarPorId(id);
        if(o == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        this.servicioProducto.guardarProductoPorId(id, p);
    }

    public static class PrecioProducto {
        public double precio;
    }

    @PatchMapping("/{id}")
    public void ajustarPrecio(@RequestHeader("Authorization") String autoriz,
                              @PathVariable String id,
                              @RequestBody PrecioProducto pp) {
        validarAutorizacion(autoriz);

        this.servicioProducto.ajustarPrecioProductoPorId(id, pp.precio);
    }

    private void validarAutorizacion(String autoriz) {
        autoriz = autoriz.trim();
        if(!autoriz.startsWith("Bearer "))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "No es un encabezado Authorization tipo Bearer Token.");

        // FIXME: validar el token como corresopnde.
        final String token = autoriz.substring(7).trim();
        try {
            final JwtClaims declaraciones = this.jwtBuilder.validarToken(token);

            if (!declaraciones.getAudience().contains(this.audiencia))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "El token no es para administrador.");

            if (!declaraciones.getSubject().equalsIgnoreCase(this.asunto))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "El token no es para este recurso.");

            if (!declaraciones.hasClaim("usuario")
                    && !declaraciones.getClaimValue("usuario").equals("andres"))
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "El token no es para un usuario reconocido.");

        } catch(ResponseStatusException x) {
            x.printStackTrace();
            throw x;
        } catch(Exception x) {
            x.printStackTrace();
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Token inválido.");
        }
    }
}
